![Alt text](/images/screenshot1.png?raw=true "screenshot1")

&nbsp;
&nbsp;

&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# my-deb-laptop-ansible 

This project customizes a laptop or a desktop from a fresh standard install of Debian GNU/linux 10 (Buster)



&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;


&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# Instructions

&nbsp;
&nbsp;

## prerequisite 
- internet connection
- a laptop or a desktop 
- (optional but highly recommended) use a usb to ethernet adapter --> to do no have wifi firmware problems during debian install and automation
- a freshly installed with Debian 10 (Buster)

    (recommend) 
     - secureboot enabled in bios
     - do not set a root password during install
     - choose encryted LVM partition during debian install
     - select gnome desktop during debian install


&nbsp;
&nbsp;

## preparation


Run thoses commands
```console

# comment cdrom line
sudo sed -i '/^deb cdrom:/ s/^/# /' /etc/apt/sources.list && sudo apt -q update 

# install prerequisites packages
sudo apt install ansible git apt-transport-https apt-transport-tor make python-pip python-jmespath python3-jmespath -y  

# get my-deb-laptop-ansible
git clone https://gitlab.com/stephane.gambus/my-deb-laptop-ansible.git

# enter in the directory downloaded
cd my-deb-laptop-ansible
```

&nbsp;
&nbsp;

## check/set parameters  

check/set parameters on [./my-deb-laptop-ansible/group_vars/all](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/group_vars/all)

**note:** you can let all parametrers by default

&nbsp;
&nbsp;


## run the automation

run the command:
```console
make
```

**note:** log files are generated in logs directory


&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;


&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# What is done (automated with ansible)

&nbsp;
&nbsp;


## role apt-configure
- remove /etc/apt/sources.list  (a backup is created in /etc/apt/sources.list~)
- create files in /etc/apt/sources.list.d/
  - stable.list                    [./my-deb-laptop-ansible/roles/apt-configure/templates/stable.list.j2](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/apt-configure/templates/stable.list.j2) 
  - backports.list                 [./my-deb-laptop-ansible/roles/apt-configure/templates/backports.list.j2](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/apt-configure/templates/backports.list.j2)
- create files in /etc/apt/preferences.d/
  - stable                         [./my-deb-laptop-ansible/roles/apt-configure/templates/stable.j2](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/apt-configure/templates/stable.j2) 
  - backports                      [./my-deb-laptop-ansible/roles/apt-configure/templates/backports.j2](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/apt-configure/templates/backports.j2)
- create /etc/apt/apt.conf.d/999seccomp
  - add line 'apt::sandbox::seccomp "true";' to enable seccomp sandbox
- update apt


&nbsp;
&nbsp;
    

## role packages
- add apt pinning priority for backports packages in /etc/apt/preferences.d/backports
- install packages from **stable** repository, listed in this file [./my-deb-laptop-ansible/roles/packages/defaults/stable.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/packages/defaults/stable.yml) 

- install packages from **backports** repository, listed in this file [./my-deb-laptop-ansible/roles/packages/defaults/backports.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/packages/defaults/backports.yml) 


&nbsp;
&nbsp;


## role mouse
- (optional) workaround to remove psmouse errors from dmesg 
  - create /etc/modprobe.d/blacklist-psmouse.conf to blacklist psmouse module

  exemple of the error message this workaround remove: **"psmouse serio1: Failed to enable mouse on isa0060/serio1"**

**note:**   to activate this workaround, change **enable_blacklist_psmouse** from false to true in group_vars/all file

**source:** https://wiki.archlinux.org/index.php/Dell_XPS_13_(9350)   in part "Remove psmouse errors from dmesg"

&nbsp;
&nbsp;


## role bluetooth
- (optional) workaround: remove the error message \"Sap driver initialization failed\" in systemctl status bluetooth.service"

  replace "ExecStart=/usr/lib/bluetooth/bluetoothd" by "ExecStart=/usr/lib/bluetooth/bluetoothd --noplugin=sap" in /lib/systemd/system/bluetooth.service

**note:** to activate this optional workaround, set **enable_bluetooth_workaround** from false to true in group_vars/all file


&nbsp;
&nbsp;


## role flatpak-configure 
- add remote repository **flathub** 

&nbsp;
&nbsp;


## role vim 
- create /etc/skel/.vimrc with lines:
    - "source /usr/share/vim/vim81/defaults.vim"
    - "set syntax=on"
    - "set mouse=r"
- copy /etc/skel/.vimrc in ~/.vimrc for root
- copy file/vimrc in ~/.vimrc for username

&nbsp;
&nbsp;

## role bash 

- for root in $HOME/.bashrc
   - uncomment aliase                 
   - uncomment "export LS_OPTIONS="   
   - uncomment "eval \"\`dircolors\`\"" 
   - insert this file at the end of the file [./my-deb-laptop-ansible/roles/bash/files/bashrc_block_to_add_for_root](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/bash/files/bashrc_block_to_add_for_root) 
- for system in /etc/skel/.bashrc
   - uncomment aliase                 
   - uncomment "force_color_prompt=yes" 
   - change     "grep --color=" from 'auto' to 'always' 
   - insert this file at the end of the file [./my-deb-laptop-ansible/roles/bash/files/bashrc_block_to_add_for_skel](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/bash/files/bashrc_block_to_add_for_skel) 

- for username in $HOME/.bashrc
   - copy (with replace) /etc/skel/.bashrc in $HOME/.bashrc 


**source:** https://damien.pobel.fr/post/bash-grep-commande-introuvable/

**source:** https://bugs.launchpad.net/ubuntu/+source/xkeyboard-config/+bug/218637

&nbsp;
&nbsp;

## role grub
- modify /etc/default/grub
   - modify GRUB_TIMEOUT=**5** to **0** 
   - ensure **splash** parameter is in GRUB_CMDLINE_LINUX_DEFAULT=
   - (optional) workaround 1, to remove message at boot time: Secure boot forbids loading module from (path)/boot/grub/*.mod"  
        this workaround run the command: grub-install --install-module=
   - (optional) workaround 2, to secure intel processor by adding parameters in GRUB_CMDLINE_LINUX_DEFAULT= on /etc/defaults/grub file
      - Provides all available mitigations for the L1TF vulnerability. Disables SMT and enables all mitigations in the hypervisors, by adding "l1tf=full"
   - update-grub

**note:**   to enable workarounds, change **enable_grub_workaround_{{ number }}** from false to true in group_vars/all file

**note:**   to access grub menu when timeout=0, hold down the Shift key at boot time just after POST

**source:** https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/l1tf.html

&nbsp;
&nbsp;

## role plymouth

- set a default theme "futureprototype"


**note:** to change the default theme, modify plymouth_theme in file group_vars/all

**note:** to know the list of avallable themes, run:  sudo plymouth-set-default-theme -l


&nbsp;
&nbsp;

## role unattended-upgrade
- modify /etc/apt/apt.conf.d/50unattended-upgrades
   - uncomment 'origin=Debian,codename=\${distro_codename}-updates' 
   - uncomment 'origin=Debian,codename=\${distro_codename}-proposed-updates' 
   - uncomment 'Unattended-Upgrade::Remove-Unused-Dependencies "false";' 
   - uncomment 'Unattended-Upgrade::AutoFixInterruptedDpkg "true"' 
- enable + unmask + start + reload unatended-upgrade.service

&nbsp;
&nbsp;

## role i915 
- check if the kernel module **i915** is loaded ( it do action only if this module is present ) 
   - create /etc/modprobe.d/i915.conf from file
   [./my-deb-laptop-ansible/roles/i915/files/i915.conf](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/i915/files/i915.conf) 

   - (optional) workaround those remove those errors messages type, when running the command sudo update-initramfs -u
     This workaround do:
       - download all firmwares on https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git
       - install missing i915 firmwares in /lib/firmware/i915/ 
       - update the initramfs 
     Example of messages we want to fix with this workaroud: 
     W: Possible missing firmware /lib/firmware/i915/icl_dmc_ver1_09.bin for module i915
     W: Possible missing firmware /lib/firmware/i915/tgl_dmc_ver2_04.bin for module i915

**note:** to activate this optional workaround,  set    enable_workaround_firmware:  true   in group_vars/all file

**note:** for more information concerning enable_guc=3, read https://01.org/linuxgraphics/downloads/firmware

&nbsp;
&nbsp;


## role sysctl 
- apply parameters presents in [./my-deb-laptop-ansible/roles/sysctl/defaults/main.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/sysctl/defaults/main.yml)

**note:** those parameters are added in file /etc/sysctl.conf


&nbsp;
&nbsp;


## role exim4 
- modify /etc/exim4/update-exim4.conf.conf
   - change dc_minimaldns='false' by 'true' to speedup boot time

&nbsp;
&nbsp;

## role tlp 
- modify /etc/tlp.conf
   - replace "^#CPU_SCALING_GOVERNOR_ON_AC=.*"    by "CPU_SCALING_GOVERNOR_ON_AC=performance"
   - replace "^#CPU_SCALING_GOVERNOR_ON_BAT=.*"   by "CPU_SCALING_GOVERNOR_ON_BAT=powersave"
   - replace "^#CPU_ENERGY_PERF_POLICY_ON_AC=.*"  by "CPU_ENERGY_PERF_POLICY_ON_AC=performance" 
   - replace "^#CPU_ENERGY_PERF_POLICY_ON_BAT=.*" by "CPU_ENERGY_PERF_POLICY_ON_BAT=balance_power" 
   - replace "^#CPU_BOOST_ON_AC=.*"               by "CPU_BOOST_ON_AC=1"
   - (optional) workaround to remove the message "xhci_hcd 0000:39:00.0: xHCI host controller not responding, assume dead" 
     - replace "^#USB_BLACKLIST_BTUSB=.*"         by "USB_BLACKLIST_BTUSB=1"
- restart tlp.service

**note:** to activate the optional workaround, set **enable_tlp_workaround** from false to true in group_vars/all file

&nbsp;
&nbsp;

## role network manager
- (optional) restore wifi configuration files in /etc/NetworkManager/system-connections/ and restart network-manager.service


**note:** to restore wifi configuration files, you need to put the backup files in ./my-deb-laptop-ansible/roles/network-manager/files/system-connections/


&nbsp;
&nbsp;

- gnome shell extensions:
   - install extensions defined in gnome_extension_target   of [./my-deb-laptop-ansible/roles/gnome/defaults/main.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/gnome/defaults/main.yml) 

   - apply parameters defined in gnomenu_extension_param    of [./my-deb-laptop-ansible/roles/gnome/defaults/main.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/gnome/defaults/main.yml) 

- gnome settings
   - apply parameters defined in gnome_gsettings            of [./my-deb-laptop-ansible/roles/gnome/defaults/main.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/gnome/defaults/main.yml) 

   - set custom desktop background                             [./my-deb-laptop-ansible/roles/gnome/files/desktop-background.jpg](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/gnome/files/desktop-background.jpg)
     - put an image in $HOME/$XDG_PICTURE_DIR/desktop-background.jpg  (exemple:  in /home/foo/Pictures/desktop-background.jpg)
     - set this image as a default desktop background

   - define the correct locale 
      ```console
      locale=$(cat /etc/default/locale | grep "^LANG=" | cut -d= -f2 | sed 's/"//g')
      gsettings set org.gnome.system.locale region "'$locale'"
      ```
   - modify the background color of gnome-terminal from white to black 
      ```console
      gsettings get org.gnome.Terminal.ProfilesList list
      gsettings get org.gnome.Terminal.ProfilesList default
      UUID=$(gsettings get org.gnome.Terminal.ProfilesList default | tr -d \')
      gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${UUID}/ use-theme-colors      'false'
      ```
      - add shortcut Ctrl+Alt+T to launch gnome-terminal whith those commands
      ```console
      gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "[ '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/' ]"
      gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name 'Terminal'
      gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command 'gnome-terminal'
      gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding '<Ctrl><Alt>T'
      ```


**note:** (optional) you can restore openweather localization settings with the **openweather_localization** param in group_vars/all file

          to get the goot value for this parameters, first set manualy the localization you want in openweather gnome applet, and then, run the command:  

          ```console
          gsettings get org.gnome.shell.extensions.openweather city
          ```


&nbsp;
&nbsp;

## role mimeapps
- copy [./my-deb-laptop-ansible/roles/mimeapps/files/mimeapps.list](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/mimeapps/files/mimeapps.list) in $HOME/.config/mimeapps.list to set :
  - vlc       as default video app
  - rhythmbox as default sound app

&nbsp;
&nbsp;


## role usbguard
- install packages listed in this file [./my-deb-laptop-ansible/roles/usbguard/defaults/main.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/usbguard/defaults/main.yml)
- modify /etc/usbguard/usbguard-daemon.conf
   - add the username in IPCAllowedUsers=
   - add the username in IPCAllowedGroups=
   - enable usbguard.service
- create $HOME/.config/USBGuard/usbguard-applet-qt.conf for user
- (optional) restore /etc/usbguard/rules.conf 

**note:** to restore the file rules.conf, you need to put the backup file in ./my-deb-laptop-ansible/roles/usbguard/files/rules.conf

**note:** for more information about usbgard, read https://usbguard.github.io/

&nbsp;
&nbsp;

## role dnscrypt-proxy
- install packages listed in this file [./my-deb-laptop-ansible/roles/dnscrypt-proxy/defaults/main.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/dnscrypt-proxy/defaults/main.yml) 
- create /etc/NetworkManager/conf.d/no-dns.conf to tell NetworkManager to do not use the dns gived by the dhcp
   - add the line "[main]"
   - add the line "dns=none"
- enable + unmask + start + reload NetworkManager.service
- replace /etc/resolv.conf to force the dns to use dnscrypt-proxy (nameserver 127.0.2.1)
- modify /etc/dnscrypt-proxy/dnscrypt-proxy.toml
   - replace server_names = ['cloudflare']  by  server_names = ['cloudflare','cloudflare-ipv6','opendns']
- enable + unmask + start + reload dnscrypt-proxy.service


**note:** The command-line tool tcpdump can be used to see if there is outgoing non-encrypted traffic

          ```console
          sudo tcpdump -n dst port 53 and 'not dst net (::1 or 10 or 127 or 172.16/12 or 192.168/16)'
          ```

&nbsp;
&nbsp;

## role  apparmor
- uncomment "write-cache" in /etc/apparmor/parser.conf   to speed-up AppArmor start by caching profiles
- enable + unmask + start + reload apparmor.service
- configure apparmor profiles&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[./my-deb-laptop-ansible/roles/apparmor/defaults/main.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/apparmor/defaults/main.yml)

**note:** source https://wiki.archlinux.org/index.php/AppArmor

&nbsp;
&nbsp;


## role firefox

**firefox ESR from debian**            (by default)
- install packages listed in this file [./my-deb-laptop-ansible/roles/firefox/defaults/main.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/firefox/defaults/main.yml) 
- add custom parameters in the user.js file of the user for firefox-esr
   - layout.css.devPixelsPerPx      1.3
   - geo.enabled                    false
   - browser.startup.homepage       https://duckduckgo.com/
   - media.gmp-gmpopenh264.enabled  true                    (needed by netflix)
   - media.eme.enabled              true                    (needed by netflix)
- (optional) restore firefox bookmarks 

**firefox latest stable from flathub** (optional) 
- install 'org.mozilla.firefox' flatpak package from flathub repository
- add custom parameters in the user.js file of the user for firefox-esr
   - layout.css.devPixelsPerPx      1.3
   - geo.enabled                    false
   - browser.startup.homepage       https://duckduckgo.com/
   - network.security.esni.enabled  true
   - network.trr.mode               3
- (optional) restore firefox bookmarks


**note:** to not install firefox from debian,  set **firefox_debian_install_enable** to **false** in group_vars/all file

**note:** to install firefox from flathub, set **firefox_flatpak_install_enable** to **true** in group_vars/all file

**note:** to restore a bookmark, you need to put the bookmarks backup file in ./my-deb-laptop-ansible/roles/firefox/files/bookmarks.sqlite3
          the same bookmark file is used for firefox-esr and firefox flatpak

**note:** to generate a bookmark backup from firefox-esr, do:
          ```console
          def_Pfile=`cat "$HOME/.mozilla/firefox/profiles.ini" | sed -n -e 's/^.*Path=//p' | head -n 1`
          sqlite3 ~/.mozilla/firefox/$def_Pfile/places.sqlite ".backup $HOME/bookmarks.sqlite3"
          ```

**note:** to generate a bookmark backup from firefox flatpak, do:
          ```console
          def_Pfile=`cat "$HOME/.var/app/org.mozilla.firefox/.mozilla/firefox/profiles.ini" | sed -n -e 's/^.*Path=//p' | head -n 1`
          sqlite3 ~/.var/app/org.mozilla.firefox/.mozilla/firefox/$def_Pfile/places.sqlite ".backup $HOME/bookmarks.sqlite3"
          ```

&nbsp;
&nbsp;

# role grammalecte-fr
- (optional)
   - download the latest version of the libreoffice grammalecte-fr plugin on https://grammalecte.net
   - install the plugin in libreoffice (with the unopkg command)

**note:** to enable grammalecte-fr, set **enable_grammalecte_fr** from false to true in group_vars/all file

&nbsp;
&nbsp;


## role docker

- (optional) install docker
            - install packages listed in this file [./my-deb-laptop-ansible/roles/docker/defaults/main.yml](https://gitlab.com/stephane.gambus/my-deb-laptop-ansible/blob/master/roles/docker/defaults/main.yml) 
            - add docker group
            - add the username in the docker group
            - restart docker.service
  
**note:** docker is needed by minikube role

**note:** to activate this role set **enable_docker** from flase to true in group_vars/all file


&nbsp;
&nbsp;

## role minikube 

Minikube is an open source tool that enables you to run Kubernetes on your laptop or other local machine. It runs a single-node cluster inside a virtual machine on your local machine.

- (optional) 
   - install kubernetes_client 
     - install kubernetes_client package from testing repository (it provide kubectl command) 
   - download and install minikube
     - download minikube script from https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
     - install minikube script in /usr/local/bin/minikube
     - add chmod +x on minikube script
   - docker pull gcr.io/k8s-minikube/kicbase image

**note:** to activate this role set **enable_minikube** from flase to true in group_vars/all file

**note:** a reboot is needed at the end of the automation before runing minikube start
**note:** to run minikube, run the command as user:   minikube start


&nbsp;
&nbsp;

## role common-end

- remove apt dependencies that were installed with applications and that are no longer used by anything else on the system. (apt autoremove)
- cleans the local repository of retrieved package files /var/cache/apt/archives/ (apt clean)
- (optional) reboot

**note:** to activate the reboot at the end of batch, change **enable_reboot** from flase to true in group_vars/all file

&nbsp;
&nbsp;

&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# Quality

the command **ansible-lint playbook.yml** return only one warning

```console
[403] Package installs should not use latest
/home/steph/my-deb-laptop-ansible/roles/packages/tasks/apt_install.yml:3
Task/Handler: install packages
```


&nbsp;
&nbsp;

&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# Annex

&nbsp;
&nbsp;


## Note about deployment on multi host

the actual configuration is done to work only on localhost (the host act as ansible client and ansible server)

If you wnat to deploy on multimachine, you need to do those actions

&nbsp;

### on all remote client hosts
  - install openssh-client openssh-server
  - configure ssh keys 

    &nbsp;
    exemple:
    &nbsp;
    # generate ssh-key
    ssh-keygen -o -a 200 -t ed25519 -f $HOME/.ssh/id_ed25519
    &nbsp;
    # copy the ssh key
    ssh-copy-id -i ~/.ssh/id_ed25519.pub $USER@hostname

&nbsp;

### on the ansible server host
  - do a first ssh connexion to all ansible client host and answer 'yes'
  - define ansible client hosts in inventories/hosts
  - remove "--connection=local" param in Makefile
&nbsp;
&nbsp;
